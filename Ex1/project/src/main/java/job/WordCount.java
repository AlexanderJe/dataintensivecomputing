package job;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import java.io.IOException;

public class WordCount{
    public static class Reduce extends Reducer<Text,Text, NullWritable,Text>{

        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException{

            try{
                JSONObject obj = new JSONObject();
                JSONArray ja = new JSONArray();
                for(Text val : values){
                    JSONObject jo = new JSONObject().put("review", val.toString());
                    ja.put(jo);
                }
                obj.put("reviews", ja);
                obj.put("category", key.toString());
                context.write(NullWritable.get(), new Text(obj.toString()));
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    public static class TokenizerMapper extends Mapper<LongWritable, Text, Text, Text> {

        private final static IntWritable ONE = new IntWritable(1);
        private Text word = new Text();

        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException{
            //StringTokenizer itr = new StringTokenizer(value.toString());

            /*while (itr.hasMoreTokens()) {
                word.set(itr.nextToken());
                if(word.getLength() > 1 && !StopWordsUtil.stopWords.contains(word.toString())) {
                    context.write(word, ONE);
                }
            }*/

            String category;
            String reviewtext;
            String line = value.toString();
            String[] tuple = line.split("\\n");
            try{
                for(int i=0;i<tuple.length; i++){
                    JSONObject obj = new JSONObject(tuple[i]);
                    category = obj.getString("category");
                    reviewtext = obj.getString("reviewText");
                    context.write(new Text(category), new Text(reviewtext));
                }
            }catch(Exception e){
                e.printStackTrace();
            }

        }
    }
}